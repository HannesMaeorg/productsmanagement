﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public DateTime Added { get; set; }
        public decimal Price { get; set; }
        public decimal PriceWithVAT { get; set; }
        public decimal VATrate { get; set; }
        [Required]
        public int GroupId { get; set; }
        [Required]
        public Group Group { get; set; }
        public List<ProductInStore> ProductsInStores { get; set; } = new List<ProductInStore>();
    }
}
