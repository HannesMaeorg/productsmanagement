﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Store
    {
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public List<ProductInStore> ProductsInStores { get; set; } = new List<ProductInStore>();
    }
}
