﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Group
    {
        public int GroupId { get; set; }
        public string Title { get; set; }

        public virtual List<Product> Products { get; set; } = new List<Product>();

        public int ParentGroupId { get; set; }
        public virtual Group ParentGroup { get; set; }

        public virtual List<Group> ChildGroups { get; set; } = new List<Group>();
    }
}
