﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class ProductInStore
    {
        public int ProductInStoreId { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int StoreId { get; set; }
        public Store Store { get; set; }
    }
}
