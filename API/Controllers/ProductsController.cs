﻿using BL.DTO;
using BL.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private readonly IProductsService _productsService;

        public ProductsController(IProductsService productsService)
        {
            _productsService = productsService;
        }

        // GET: api/Products
        [HttpGet]
        public List<ProductDTO> GetAll() => _productsService.GetAll();

        // POST: api/Products
        [HttpPost]
        public IActionResult Add([FromBody]ProductDTO dto)
        {
            if (!ModelState.IsValid) return BadRequest("Model state invalid");

            var product = _productsService.Add(dto);
            if (product == null) return BadRequest("Product is already in database");
            return Ok("Product " + dto.Name + " added");
        }

        // GET: api/Products/{id}
        [HttpGet("{id:int}")]
        public IActionResult GetById(int id)
        {
            var product = _productsService.GetById(id);
            if (product == null) return NotFound("No product with id " + id + " found");
            return Ok(product);
        }
    }
}
