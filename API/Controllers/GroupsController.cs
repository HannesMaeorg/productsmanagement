﻿using BL.DTO;
using BL.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class GroupsController : Controller
    {
        private readonly IGroupsService _groupService;

        public GroupsController(IGroupsService groupService)
        {
            _groupService = groupService;
        }

        // GET: api/Groups
        [HttpGet]
        public List<GroupDTO> GetAll() => _groupService.GetAll();

        // POST: api/Groups
        [HttpPost]
        public IActionResult Add([FromBody]GroupDTO dto)
        {
            if (!ModelState.IsValid) return BadRequest("Model state invalid");

            var group = _groupService.Add(dto);
            if (group == null) return BadRequest("Group is already in database");
            return Ok("Group " + dto.Title + " added");
        }

        // GET: api/Groups/{id}
        [HttpGet("{id:int}")]
        public IActionResult GetById(int id)
        {
            var group = _groupService.GetById(id);
            if (group == null) return NotFound("No group with id " + id + " found");
            return Ok(group);
        }
    }
}
