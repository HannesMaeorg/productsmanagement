﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL.DTO;
using BL.IServices;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class StoresController : ControllerBase
    {
        private readonly IStoresService _storesService;

        public StoresController(IStoresService storesService)
        {
            _storesService = storesService;
        }

        // GET: api/Stores
        [HttpGet]
        public List<StoreDTO> GetAll() => _storesService.GetAll();

        // POST: api/Stores
        [HttpPost]
        public IActionResult Add([FromBody]StoreDTO dto)
        {
            if (!ModelState.IsValid) return BadRequest("Model state invalid");

            var store = _storesService.Add(dto);
            if (store == null) return BadRequest("Store is already in database");
            return Ok("Store " + dto.StoreName + " added");
        }

        // GET: api/Stores/{id}
        [HttpGet("{id:int}")]
        public IActionResult GetById(int id)
        {
            var store = _storesService.GetById(id);
            if (store == null) return NotFound("No store with id " + id + " found");
            return Ok(store);
        }
    }
}
