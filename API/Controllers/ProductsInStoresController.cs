﻿using BL.DTO;
using BL.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class ProductsInStoresController : Controller
    {
        private readonly IProductsInStoresService _productsInStoresService;

        public ProductsInStoresController(IProductsInStoresService productsInStoresService)
        {
            _productsInStoresService = productsInStoresService;
        }

        // GET: api/ProductsInStores
        [HttpGet]
        public List<ProductInStoreDTO> GetAll() => _productsInStoresService.GetAll();

        // POST: api/ProductsInStores
        [HttpPost]
        public IActionResult Add([FromBody]ProductInStoreDTO dto)
        {
            if (!ModelState.IsValid) return BadRequest("Model state invalid");

            var productInStore = _productsInStoresService.Add(dto);
            if (productInStore == null) return BadRequest("ProductInStore is already in database");
            return Ok("ProductInStore with id  " + productInStore.ProductInStoreId + " added");
        }

        // GET: api/ProductsInStores/{id}
        [HttpGet("{id:int}")]
        public IActionResult GetById(int id)
        {
            var productInStore = _productsInStoresService.GetById(id);
            if (productInStore == null) return NotFound("No productInStore with id " + id + " found");
            return Ok(productInStore);
        }

        // DELETE: api/ProductsInStores/{id}
        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            bool ProductsInStores = _productsInStoresService.Delete(id);
            if (ProductsInStores == false) return BadRequest("ProductInStore with id " + id + " not found");
            return Ok("ProductInStore with id " + id + " deleted!");

        }
    }
}
