﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.App.Interfaces.Repositories;
using DAL.Interfaces;
using DAL.Interfaces.Repositories;
using Domain;

namespace DAL.App.Interfaces
{
    public interface IAppUnitOfWork : IUnitOfWork
    {
        IRepository<Product> Products { get; }
        IRepository<Group> Groups { get; }
        IRepository<Store> Stores { get; }
        IRepository<ProductInStore> ProductsInStores { get; }

    }
}
