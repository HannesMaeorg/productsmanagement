﻿using BL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BL.IServices
{
    public interface IStoresService
    {
        List<StoreDTO> GetAll();

        StoreDTO GetById(int id);

        StoreDTO Add(StoreDTO dto);
    }
}
