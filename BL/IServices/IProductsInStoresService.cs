﻿using BL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BL.IServices
{
    public interface IProductsInStoresService
    {
        List<ProductInStoreDTO> GetAll();

        ProductInStoreDTO GetById(int id);

        ProductInStoreDTO Add(ProductInStoreDTO dto);

        bool Delete(int id);
    }
}
