﻿using BL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BL.IServices
{
    public interface IProductsService
    {
        List<ProductDTO> GetAll();

        ProductDTO GetById(int id);

        ProductDTO Add(ProductDTO dto);
    }
}
