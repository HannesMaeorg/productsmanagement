﻿using BL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BL.IServices
{
    public interface IGroupsService
    {
        List<GroupDTO> GetAll();

        GroupDTO GetById(int id);

        GroupDTO Add(GroupDTO dto);
    }
}
