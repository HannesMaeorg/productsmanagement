﻿using BL.DTO;
using BL.IFactories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL.Factories
{
    public class StoreFactory : IStoreFactory
    {
        private readonly IProductInStoreFactory _productInStoreFactory;

        public StoreFactory(IProductInStoreFactory productInStoreFactory)
        {
            _productInStoreFactory = productInStoreFactory;
        }

        public StoreDTO Transform(Store s)
        {
            return new StoreDTO
            {
                StoreId = s.StoreId,
                StoreName = s.StoreName
            };
        }

        public Store Transform(StoreDTO dto)
        {
            return new Store
            {
                StoreId = dto.StoreId,
                StoreName = dto.StoreName
            };
        }

        public StoreDTO TransformWithProducts(Store s)
        {
            var dto = Transform(s);
            if (dto == null) return null;
            dto.ProductsInStores = s?.ProductsInStores
                .Select(c => _productInStoreFactory.TransformWithProducts(c)).ToList();
            return dto;
        }
    }
}
