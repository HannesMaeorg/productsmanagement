﻿using BL.DTO;
using BL.IFactories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL.Factories
{
    public class GroupFactory : IGroupFactory
    {
        private readonly IProductFactory _productFactory;

        public GroupFactory(IProductFactory productFactory)
        {
            _productFactory = productFactory;
        }

        public GroupDTO Transform(Group g)
        {
            return new GroupDTO
            {
                GroupId = g.GroupId,
                Title = g.Title,
                ParentGroupId = g.ParentGroupId

            };
        }

        public Group Transform(GroupDTO dto)
        {
            return new Group()
            {
                GroupId = dto.GroupId,
                Title = dto.Title,
                ParentGroupId = dto.ParentGroupId

            };
        }

        public GroupDTO TransformWithChildGroups(Group g)
        {
            var dto = Transform(g);
            if (dto == null) return null;
            dto.ChildGroups = g?.ChildGroups
                .Select(c => TransformWithChildGroups(c)).ToList();
            return dto;
        }

        public GroupDTO TransformWithProducts(Group g)
        {
            var dto = Transform(g);
            if (dto == null) return null;
            dto.Products = g?.Products
                .Select(c => _productFactory.Transform(c)).ToList();
            return dto;
        }
    }
}
