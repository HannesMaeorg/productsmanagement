﻿using BL.DTO;
using BL.IFactories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL.Factories
{
    public class ProductFactory : IProductFactory
    {
        private readonly IProductInStoreFactory _productInStoreFactory;

        public ProductFactory(IProductInStoreFactory productInStoreFactory)
        {
            _productInStoreFactory = productInStoreFactory;
        }

        public ProductDTO Transform(Product p)
        {
            return new ProductDTO
            {
                ProductId = p.ProductId,
                Name = p.Name,
                Added = p.Added,
                Price = p.Price,
                PriceWithVAT = p.PriceWithVAT,
                VATrate = p.VATrate,
                GroupId = p.GroupId,
            };
        }

        public Product Transform(ProductDTO dto)
        {
            return new Product
            {
                ProductId = dto.ProductId,
                Name = dto.Name,
                Added = dto.Added,
                Price = dto.Price,
                PriceWithVAT = dto.PriceWithVAT,
                VATrate = dto.VATrate,
                GroupId = dto.GroupId
            };
        }

        public ProductDTO TransformWithGroup(Product p)
        {
            var dto = Transform(p);
            if (dto == null) return null;
            dto.Group = GroupDTO.CreateFromDomain(p.Group);
            return dto;
        }

        public ProductDTO TransformWithStores(Product p)
        {
            var dto = Transform(p);
            if (dto == null) return null;
            dto.ProductsInStores = p?.ProductsInStores
                .Select(c => _productInStoreFactory.TransformWithStores(c)).ToList();
            return dto;
        }

        public ProductDTO TransformWithAllObjects(Product p)
        {
            var dto = TransformWithGroup(p);
            if (dto == null) return null;
            dto.ProductsInStores = p?.ProductsInStores
                .Select(c => _productInStoreFactory.TransformWithStores(c)).ToList();
            return dto;

        }
    }
}
