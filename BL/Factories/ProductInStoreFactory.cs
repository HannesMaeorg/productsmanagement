﻿using BL.DTO;
using BL.IFactories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Factories
{
    public class ProductInStoreFactory : IProductInStoreFactory
    {
        //private readonly IProductFactory _productFactory;
        //private readonly IStoreFactory _storeFactory;

        //public ProductInStoreFactory(IProductFactory productFactory, IStoreFactory storeFactory)
        //{
        //    _productFactory = productFactory;
        //    _storeFactory = storeFactory;
        //}


        public ProductInStoreDTO Transform(ProductInStore ps)
        {
            return new ProductInStoreDTO
            {
                ProductInStoreId = ps.ProductInStoreId,
                ProductId = ps.ProductId,
                StoreId = ps.ProductInStoreId,
            };
        }

        public ProductInStore Transform(ProductInStoreDTO dto)
        {
            return new ProductInStore
            {
                ProductInStoreId = dto.ProductInStoreId,
                ProductId = dto.ProductId,
                StoreId = dto.ProductInStoreId,
            };
        }

        public ProductInStoreDTO TransformWithProducts(ProductInStore ps)
        {
            //throw new NotImplementedException();

            //var dto = Transform(ps);
            //if (dto == null) return null;
            //dto.Product = _productFactory.Transform(ps?.Product);
            //return dto;

            var dto = Transform(ps);
            if (dto == null) return null;
            dto.Product = ProductDTO.CreateFromDomain(ps.Product);
            return dto;
        }

        public ProductInStoreDTO TransformWithStores(ProductInStore ps)
        {
            //throw new NotImplementedException();
            //var dto = Transform(ps);
            //if (dto == null) return null;
            //dto.Store = _storeFactory.Transform(ps?.Store);
            //return dto;

            var dto = Transform(ps);
            if (dto == null) return null;
            dto.Store = StoreDTO.CreateFromDomain(ps.Store);
            return dto;
        }
    }
}
