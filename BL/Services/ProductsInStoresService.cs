﻿using BL.DTO;
using BL.IFactories;
using BL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public class ProductsInStoresService : IProductsInStoresService
    {
        private readonly DAL.App.Interfaces.IAppUnitOfWork _uow;
        private readonly IProductInStoreFactory _productInStoreFactory;


        public ProductsInStoresService(DAL.App.Interfaces.IAppUnitOfWork uow, IProductInStoreFactory productInStoreFactory)
        {
            _uow = uow;
            _productInStoreFactory = productInStoreFactory;
        }

        public ProductInStoreDTO Add(ProductInStoreDTO dto)
        {
            var productInStore = _productInStoreFactory.Transform(dto);
            _uow.ProductsInStores.Add(productInStore);
            _uow.SaveChanges();
            return _productInStoreFactory.Transform(productInStore);
        }

        public List<ProductInStoreDTO> GetAll()
        {
            return _uow.ProductsInStores.All().Select(g => _productInStoreFactory.Transform(g)).ToList();
        }

        public ProductInStoreDTO GetById(int id)
        {
            var productInStore = _uow.ProductsInStores.Find(id);
            if (productInStore == null)
            {
                return null;
            }
            return _productInStoreFactory.Transform(productInStore);
        }

        public bool Delete(int id)
        {
            var productInStore = _uow.ProductsInStores.Find(id);
            if (productInStore == null)
            {
                return false;
            }
            _uow.ProductsInStores.Detach(productInStore);
            productInStore.ProductInStoreId = id;
            _uow.ProductsInStores.Remove(productInStore);
            _uow.SaveChanges();
            return true;
        }
    }
}
