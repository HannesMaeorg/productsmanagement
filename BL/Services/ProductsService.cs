﻿using BL.DTO;
using BL.IFactories;
using BL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public class ProductsService : IProductsService
    {
        private readonly DAL.App.Interfaces.IAppUnitOfWork _uow;
        private readonly IProductFactory _productFactory;


        public ProductsService(DAL.App.Interfaces.IAppUnitOfWork uow, IProductFactory productFactory)
        {
            _uow = uow;
            _productFactory = productFactory;
        }

        public ProductDTO Add(ProductDTO dto)
        {
            var product = _productFactory.Transform(dto);

            if(product.Price == 0 && product.PriceWithVAT != 0 && product.VATrate != 0)
            {
                product.Price = product.PriceWithVAT -  (product.VATrate * product.PriceWithVAT / (product.VATrate + 100));
            }
            else if(product.Price != 0 && product.PriceWithVAT == 0 && product.VATrate != 0)
            {
                product.PriceWithVAT = product.Price + (product.VATrate * product.Price / 100);
            }
            else if (product.Price != 0 && product.PriceWithVAT != 0 && product.VATrate == 0)
            {
                product.VATrate = (product.PriceWithVAT - product.Price) * 100 / product.Price;
            }
            product.Added = DateTime.Now;
            _uow.Products.Add(product);
            _uow.SaveChanges();
            return _productFactory.Transform(product);
        }

        public List<ProductDTO> GetAll()
        {
            return _uow.Products.All().Select(g => _productFactory.TransformWithAllObjects(g)).ToList();
        }

        public ProductDTO GetById(int id)
        {
            var product = _uow.Products.Find(id);
            if (product == null)
            {
                return null;
            }
            return _productFactory.TransformWithAllObjects(product);
        }
    }
}
