﻿using BL.DTO;
using BL.IFactories;
using BL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public class StoresService : IStoresService
    {
        private readonly DAL.App.Interfaces.IAppUnitOfWork _uow;
        private readonly IStoreFactory _storeFactory;


        public StoresService(DAL.App.Interfaces.IAppUnitOfWork uow, IStoreFactory storeFactory)
        {
            _uow = uow;
            _storeFactory = storeFactory;
        }

        public StoreDTO Add(StoreDTO dto)
        {
            var store = _storeFactory.Transform(dto);
            _uow.Stores.Add(store);
            _uow.SaveChanges();
            return _storeFactory.Transform(store);
        }


        public List<StoreDTO> GetAll()
        {
            return _uow.Stores.All().Select(g => _storeFactory.TransformWithProducts(g)).ToList();
        }

        public StoreDTO GetById(int id)
        {
            var store = _uow.Stores.Find(id);
            if (store == null)
            {
                return null;
            }
            return _storeFactory.TransformWithProducts(store);
        }
    }
}
