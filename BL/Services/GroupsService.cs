﻿using BL.DTO;
using BL.IFactories;
using BL.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public class GroupsService : IGroupsService
    {
        private readonly DAL.App.Interfaces.IAppUnitOfWork _uow;
        private readonly IGroupFactory _groupFactory;


        public GroupsService(DAL.App.Interfaces.IAppUnitOfWork uow, IGroupFactory groupFactory)
        {
            _uow = uow;
            _groupFactory = groupFactory;
        }

        public GroupDTO Add(GroupDTO dto)
        {
            var group = _groupFactory.Transform(dto);
            _uow.Groups.Add(group);
            _uow.SaveChanges();
            return _groupFactory.Transform(group);
        }

        public List<GroupDTO> GetAll()
        {
            return _uow.Groups.All().Select(g => _groupFactory.TransformWithChildGroups(g)).ToList().Where(g => g.ParentGroupId == 0).ToList();
        }


        public GroupDTO GetById(int id)
        {
            var group = _uow.Groups.Find(id);
            if (group == null)
            {
                return null;
            }
            return _groupFactory.TransformWithProducts(group);
        }
    }
}
