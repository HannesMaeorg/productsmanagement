﻿namespace BL.DTO
{
    public class ProductInStoreDTO
    {
        public int ProductInStoreId { get; set; }
        public int ProductId { get; set; }
        public ProductDTO Product { get; set; }
        //public string ProductName { get; set; }
        public int StoreId { get; set; }
        public StoreDTO Store { get; set; }
        //public string StoreName { get; set; }
    }
}