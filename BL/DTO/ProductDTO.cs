﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BL.DTO
{
    public class ProductDTO
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public DateTime Added { get; set; }
        public decimal Price { get; set; }
        public decimal PriceWithVAT { get; set; }
        public decimal VATrate { get; set; }
        [Required]
        public int GroupId { get; set; }
        public GroupDTO Group { get; set; }
        public List<ProductInStoreDTO> ProductsInStores { get; set; } = new List<ProductInStoreDTO>();

        internal static ProductDTO CreateFromDomain(Product p)
        {
            if (p == null) return null;
            return new ProductDTO
            {
                ProductId = p.ProductId,
                Name = p.Name
            };
        }
    }
}