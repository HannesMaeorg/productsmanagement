﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class GroupDTO
    {
        public int GroupId { get; set; }
        public string Title { get; set; }

        public virtual List<ProductDTO> Products { get; set; } = new List<ProductDTO>();

        public int ParentGroupId { get; set; }

        public virtual List<GroupDTO> ChildGroups { get; set; } = new List<GroupDTO>();


        internal static GroupDTO CreateFromDomain(Group g)
        {
            if (g == null) return null;
            return new GroupDTO
            {
                GroupId = g.GroupId,
                Title = g.Title
            };
        }
    }
}
