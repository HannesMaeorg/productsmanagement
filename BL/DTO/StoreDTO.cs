﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain;

namespace BL.DTO
{
    public class StoreDTO
    {
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public List<ProductInStoreDTO> ProductsInStores { get; set; } = new List<ProductInStoreDTO>();

        internal static StoreDTO CreateFromDomain(Store s)
        {
            if (s == null) return null;
            return new StoreDTO
            {
                StoreId = s.StoreId,
                StoreName = s.StoreName
            };
        }
    }
}
