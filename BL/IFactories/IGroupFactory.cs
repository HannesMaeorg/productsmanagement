﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.IFactories
{
    public interface IGroupFactory
    {
        GroupDTO Transform(Group g);
        Group Transform(GroupDTO dto);
        GroupDTO TransformWithChildGroups(Group g);
        GroupDTO TransformWithProducts(Group g);
    }
}
