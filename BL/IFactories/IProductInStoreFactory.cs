﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.IFactories
{
    public interface IProductInStoreFactory
    {
        ProductInStoreDTO Transform(ProductInStore ps);
        ProductInStore Transform(ProductInStoreDTO dto);
        ProductInStoreDTO TransformWithProducts(ProductInStore ps);
        ProductInStoreDTO TransformWithStores(ProductInStore ps);
    }
}
