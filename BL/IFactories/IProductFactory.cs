﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.IFactories
{
    public interface IProductFactory
    {
        ProductDTO Transform(Product p);
        Product Transform(ProductDTO dto);
        ProductDTO TransformWithGroup(Product p);
        ProductDTO TransformWithStores(Product p);
        ProductDTO TransformWithAllObjects(Product p);
    }
}
