﻿using BL.DTO;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.IFactories
{
    public interface IStoreFactory
    {
        StoreDTO Transform(Store s);
        Store Transform(StoreDTO dto);
        StoreDTO TransformWithProducts(Store s);
    }
}
